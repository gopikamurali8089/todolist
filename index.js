function addtask()
{
    let input=document.getElementById("inputtext");
    let task=input.value.trim();
    if (task === "")
        {
         
        
        
        alert("Please enter a task!");
        return;
        }
    let ul=document.getElementById("listcontainer");
    let li=document.createElement("li");
    li.innerHTML='<input type="checkbox" onclick="completed(this)">'+task+'<button class="delete-btn" onclick="removetask(this.parentNode)">X</button>';
    ul.appendChild(li);
    input.value="";
}
function removetask(taskItem)
{
    taskItem.parentNode.removeChild(taskItem);

}
function completed(checkbox) {
    if (checkbox.checked) {
      checkbox.parentNode.style.textDecoration = "line-through";
    } else {
      checkbox.parentNode.style.textDecoration = "none";
    }
  }